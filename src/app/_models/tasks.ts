﻿export class Task {
    id: number;
    title: string;
    status: string;
    group: string;
    description: string;
    created_at: string;
}