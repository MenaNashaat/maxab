import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';import { Task} from '@app/_models'
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators ,NgForm} from '@angular/forms';
import { first } from 'rxjs/operators';
import { TasksService } from '@app/_services';

@Component({
  selector: 'app-show-task',
  templateUrl: './show-task.component.html',
  styleUrls: ['./show-task.component.css']
})
export class ShowTaskComponent implements OnInit {
  mena: Task[];
  constructor(private tasksService: TasksService,
    @Inject(MAT_DIALOG_DATA) public data,
  public dialogRef: MatDialogRef<ShowTaskComponent>) { }
  id; title; group; created_at;status; description : any;
  ngOnInit() {
     this.mena = JSON.parse(JSON.stringify( this.data.info));
    this.id= this.mena[0].id;
    this.title= this.mena[0].title;
    this.description= this.mena[0].description;
    this.status= this.mena[0].status;
    this.group= this.mena[0].group;
    this.created_at= this.mena[0].created_at;
  }

}
