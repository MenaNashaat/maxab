import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Task } from '@app/_models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TasksService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get<Task[]>("http://mena-nashaat.herokuapp.com/task/getAll");
  }
  delete(id): Observable<Task[]> {
  
    return this.http.delete<Task[]>(`http://mena-nashaat.herokuapp.com/task/${id}` , this.httpOptions);
  }
  getById(id): Observable<Task[]> {
  
    return this.http.get<Task[]>(`http://mena-nashaat.herokuapp.com/task/${id}` , this.httpOptions);
  }
  
  createTask(title: string, description: string, status: string, group: string , created_at: string) {
    return this.http.post<any>(`http://mena-nashaat.herokuapp.com/task/addTask`, { title,description,status,group,created_at })
  }
  updatetask (id, title: string, description: string, status: string, group: string , created_at: string): Observable<Task[]> {
   
    return this.http.put<Task[]>(`http://mena-nashaat.herokuapp.com/task/${id}`,  { title,description,status,group,created_at })
   
  }
}
