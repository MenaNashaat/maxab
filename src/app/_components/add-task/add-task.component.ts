import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators , NgForm} from '@angular/forms';
import { first } from 'rxjs/operators';

import { TasksService } from '@app/_services';
@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private tasksService: TasksService,
    public dialogRef: MatDialogRef<AddTaskComponent>
   ) { }

  ngOnInit() {
   
    this.loginForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      createdAt: ['', Validators.required]
  });

  }
    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {

        console.log(" this.f.createdAt.value this.f.createdAt.value" + this.f.createdAt.value)
        this.tasksService.createTask(this.f.title.value, this.f.description.value , this.f.status.value , this.f.group.value ,  this.f.createdAt.value).pipe(first()).subscribe(() => {
        this.closeDialog()
      });
    }
    closeDialog() {
      this.dialogRef.close(false);
    }
   

}
