import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Task} from '@app/_models'
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators ,NgForm} from '@angular/forms';
import { first } from 'rxjs/operators';
import { TasksService } from '@app/_services';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {
  date = new FormControl(new Date());
  date1 = "2019-12-29T19:17:23.351Z"

  //serializedDate = new FormControl((new Date()).toISOString());

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
   mena: Task[];
   variable:string;
   
  
  
  constructor(
    private formBuilder: FormBuilder,
    private tasksService: TasksService,
    @Inject(MAT_DIALOG_DATA) public data,
  public dialogRef: MatDialogRef<EditTaskComponent>) { }
 id; title; group; created_at;status; description : any;
  ngOnInit() {
    // console.log("date" + JSON.stringify(this.date));
    //console.log("serializedDate" + JSON.stringify(this.serializedDate));
    this.mena = JSON.parse(JSON.stringify( this.data.info));
    this.id= this.mena[0].id;
    this.title= this.mena[0].title;
    this.description= this.mena[0].description;
    this.status= this.mena[0].status;
    this.group= this.mena[0].group;
    this.created_at= this.mena[0].created_at;
    

    console.log("amany " +this.created_at);
    
    this.loginForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      createdAt: ['', Validators.required]
  });
    console.log("amany " +this.status);
    console.log("mena 1 " +JSON.stringify(this.mena));
    
    console.log("mena 1 " +JSON.stringify(this.mena));
    
      
  }
 // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
       
       let title = (<HTMLInputElement>document.getElementById("title")).value;
       let description = (<HTMLInputElement>document.getElementById("description")).value;
       let status = (<HTMLInputElement>document.getElementById("status")).value;
       let group = (<HTMLInputElement>document.getElementById("group")).value;
       let createdAt = (<HTMLInputElement>document.getElementById("createdAt")).value;
      console.log("createdAt" + createdAt)
        // console.log("mensdsdsd" + JSON.stringify(form))
        
        this.tasksService.updatetask(this.id,  title, description , status , group,  createdAt).pipe(first()).subscribe(() => {
          this.closeDialog()
      });
    }
    closeDialog() {
      this.dialogRef.close(false);
    }
   
  
}
