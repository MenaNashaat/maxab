﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// used to create fake backend
import { fakeBackendProvider } from './_helpers';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { AppComponent }  from './app.component';
import { routing }        from './app.routing';

import { AlertComponent } from './_components';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { MaterialModule } from './material/material.module';
import { MatConfirmDialogComponent } from './_components/mat-confirm-dialog/mat-confirm-dialog.component';
import { AddTaskComponent } from './_components/add-task/add-task.component';
import { EditTaskComponent } from './_components/edit-task/edit-task.component';
import { FormsModule } from '@angular/forms';;
import { ShowTaskComponent } from './_components/show-task/show-task.component'

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        routing,
        MaterialModule,
        FormsModule, 
        DragDropModule
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        MatConfirmDialogComponent ,
        AddTaskComponent ,
        EditTaskComponent ,
        ShowTaskComponent 
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent],
    entryComponents:[MatConfirmDialogComponent, AddTaskComponent, EditTaskComponent, ShowTaskComponent]
})

export class AppModule { }