﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { first } from 'rxjs/operators';
import { AddTaskComponent } from '../_components/add-task/add-task.component';
import { EditTaskComponent } from '../_components/edit-task/edit-task.component';
import { ShowTaskComponent } from '../_components/show-task/show-task.component';
import { User } from '@app/_models';
import { Task } from '@app/_models'
import { UserService, TasksService, AuthenticationService, DialogService } from '@app/_services';

import { MatDialog, MatDialogConfig } from "@angular/material";


@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit, OnDestroy {

  currentUser: User;
  currentUserSubscription: Subscription;
  users: User[] = [];
  tasks: Task[] = [];

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private tasksService: TasksService,
    private dialogService: DialogService,
    private dialog: MatDialog,
  ) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.loadAllUsers();
    this.loadAlltasks();
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.currentUserSubscription.unsubscribe();
  }

  deleteUser(id: number) {
    this.userService.delete(id).pipe(first()).subscribe(() => {
      this.loadAllUsers()
    });
  }
  deleteTask(id: number) {

    this.tasksService.delete(id).pipe(first()).subscribe(() => {
      this.loadAlltasks()
    });
  }
  onDelete(id) {
    this.dialogService.openConfirmDialog('Are you sure to delete this record ?')
      .afterClosed().subscribe(res => {

        if (res) {
          this.tasksService.delete(id).pipe(first()).subscribe(() => {
            this.loadAlltasks()
          });
          //this.service.deleteEmployee($key);
          // this.notificationService.warn('! Deleted successfully');
        }
      });
  }
  onEdit(id) {
    this.tasksService.getById(id).pipe(first()).subscribe(info => {
      console.log("edit res" + JSON.stringify(info));
      this.dialog.open(EditTaskComponent, { data: { info }, width: '450px' })
        .afterClosed().subscribe(res => {
          console.log(" res" + res);
          this.loadAlltasks()
          //  if(res){
          //    this.loadAlltasks()
          //  }
        });
    });
  }
  onShow(id) {
    this.tasksService.getById(id).pipe(first()).subscribe(info => {
      console.log("edit res" + JSON.stringify(info));
      this.dialog.open(ShowTaskComponent, {
        data: { info }, width: '390px',
      })
        .afterClosed().subscribe(res => {
          console.log(" res" + res);
          this.loadAlltasks()
          //  if(res){
          //    this.loadAlltasks()
          //  }
        });
    });
  }
  onCreate() {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "100%";
    this.dialog.open(AddTaskComponent, {
      width: '450px'
    })
      .afterClosed().subscribe(res => {
        console.log(" res" + res);
        this.loadAlltasks()
        if (res) {
          this.loadAlltasks()
        }
      });
  }

  public loadAllUsers() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }
  private loadAlltasks() {
    this.tasksService.getAll().pipe(first()).subscribe(tasks => {
      this.tasks = tasks;
    });
  }



}